import pandas as pd
import numpy as np
import pytest

@pytest.fixture()
def get_df():
    return pd.DataFrame([[1, None, None], [3, None, None]], columns=['a', 'b', 'c'])

@pytest.fixture()
def get_df_expected():
    return pd.DataFrame({'a': {0: 1, 1: 3}, 'b': {0: np.nan, 1: np.nan}, 'c': {0: np.nan, 1: np.nan}})