import pytest

def return_double(x):
    return x * 2

def test_return_double():
    assert return_double(0) == 0
    assert return_double(1) == 2
    assert return_double(2) == 4
    assert return_double(3) == 6
    
@pytest.mark.parametrize('val, res', 
                         [(0, 0),
                          (1, 2),
                          (2, 4),
                          (3,  6)])
def test_parametrized(val, res):
    assert return_double(val) == res