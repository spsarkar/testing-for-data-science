{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Unit testing with PyTest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## PyTest syntax"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PyTest can be run on a directory, py file, or function. The syntax is the same as for the -v output:\n",
    "\n",
    "`pytest folder file.py file.py::function`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test discovery: PyTest, run with no arguments or with a directory, searches recursively for functions to test in subdirectories according to the naming convention:\n",
    "     * files are named `test _ (). py` or` () _test.py`,\n",
    "     * test functions are called `test _ ()`,\n",
    "     * test classes Test ()."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Positive tests are written through the `assert boolean expression`. It is possible to write several asserts in one test. Negatives via with `pytest.raises (TypeError)`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PyTest Statuses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Status | Status | Description | Using |\n",
    "|-------------|--------|----------|---------------|\n",
    "| `.`  | `Passed` | Successful test ||\n",
    "| `s` | `Skips` | Test skipped | `@pytest.mark.skip( [reason] )` |\n",
    "|||| `@pytest.mark.skipif( condition, [reason] )` |\n",
    "|   `x`  | `xfails` | Negative test failed | `@pytest.mark.xfail()` |\n",
    "| `F` | `Failure` | Test failed ||\n",
    "| `X` | `XPasses` | Negative test passed | `@pytest.mark.xfail()` |\n",
    "| `E` | `Error` | Error, but not in the test assert ||\n",
    "|||||"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Errors are indicated in uppercase letters, expected behavior in lowercase."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## PyTest Flags"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using Flags:\n",
    "\n",
    "```python\n",
    "pytest flag ... flag\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Frequent"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "| Flag | Description   | Use |\n",
    "|------|---------------|---------------|\n",
    "| `-v`  | Print more information ||\n",
    "| `--verbose` |||\n",
    "|   `-q`  | Print less information ||\n",
    "| `--collect-only` | Collect tests, but don't run | Check flags ||\n",
    "|||For example, when choosing a subset of tests|\n",
    "| `--tb=no` | Disable stack trace | Don't display info |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Select a subset of tests"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Flag | Description  | Use |\n",
    "|------|---------------|---------------|\n",
    "| `-k Expression`  | Subset by name ||\n",
    "| `-m Marker` | Subset by marker | `@pytest.mark.Маркер` |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to use the logical operators or, and and not in both Expressions and Markers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Other useful"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "| Flag | Description | Use |\n",
    "|------|---------------|---------------|\n",
    "| `-x`  | Test before the first error ||\n",
    "| `--exitfirst` |||\n",
    "|   `--maxfail=num`  | Test up to num errors ||\n",
    "| `-s` | Print stdout on successful tests | Same as `--capture=no` ||\n",
    "| `-l` | Show local variables ||\n",
    "| `-lf` | Start testing from previous negative test | While debug |\n",
    "| `--last-fail` |||\n",
    "| `--durations=num` | With num = 0, it shows the running time of all tests |||\n",
    "| `-r[f / p / x]` | Shows additional information | Depends on the type of test ||\n",
    "| `--fixtures` | View available fixtures | Run with only one flag |\n",
    "| `--setup-show` | View __WHEN__ Fixtures Running ||"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fixtures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Users fixtures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is easier to define fixtures through their action. There are 2 kinds of fixtures in PyTest:\n",
    "    * Fixtures - decorators\n",
    "    * Fixtures - data substitutes\n",
    "    \n",
    "A fixture is a function in test code with the @ pytest.fixture () decorator or a class with the `@ pytest.mark.usefixture` decorator ('fixture'). It has a scope:\n",
    "\n",
    "    * session `scope = 'session'`\n",
    "    * module `scope = 'module'`\n",
    "    * class `scope = 'class'`\n",
    "    * function (default)\n",
    "and for a scope, the fixture runs once and acts within the scope."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is good practice to put fixtures in a separate conftest.py file in the root tests folder, from there they will be recursively available in all subdirectories. It is not a module, but a plugin. It does not need to be imported in test modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fixture syntax"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pytest\n",
    "\n",
    "@pytest.fixture()\n",
    "def this_is_data_fixture():\n",
    "    return fixture_value"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pytest\n",
    "\n",
    "@pytest.fixture()\n",
    "def this_is_decorator_fixture():\n",
    "    # Run this BEFORE test scope\n",
    "    ...\n",
    "    \n",
    "    yeild\n",
    "    \n",
    "    # Run this AFTER test scope\n",
    "    ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fixture from fixture is a fixture."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Built-in fixtures"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fixture __`tmpdir`__ and __`tmpdir-factory`__.\n",
    "\n",
    "Relevant if different test functions use same file.\n",
    "\n",
    "The file can also be created for `read_csv`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__Capsys__ - capture of streams stdout, stderr.\n",
    "\n",
    "PyTest captures the output stream for successful functions.\n",
    "\n",
    "Can be bypassed with the -s flag or with\n",
    "```python\n",
    "with capsys.disabled():\n",
    "    print('Print it in Passed Test functions.')\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Fixture __doctest_namespace__ for not writing to doctest:\n",
    "    `>>import pandas as pd`\n",
    "\n",
    "possible to use an autostart fixture:\n",
    "\n",
    "```python\n",
    "import pandas\n",
    "\n",
    "@pytest.fixture(autouse=True)\n",
    "def add_pd(doctest_namespace):\n",
    "    doctest_namespace['pd'] = pandas\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Testing_Py36",
   "language": "python",
   "name": "testing_py36"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
